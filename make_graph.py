from matplotlib import pyplot as plt

f = open('original.txt')
f2 = open('smooth.txt')

orig = list(map(float, f.read().split()))
smooth = list(map(float, f2.read().split()))

plt.plot(orig, label='Original weather')
plt.plot(smooth, label='Smooth weather')

plt.title("Usual weather and smooth")
plt.xlabel("Hours")
plt.ylabel("Temperature, C")
plt.legend()
plt.savefig('graph.jpg')