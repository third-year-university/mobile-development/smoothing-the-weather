import java.io.File
import java.util.*

fun arrayToOutput(array: DoubleArray) : String {
    var str = ""
    for (elem in array){
        str += "$elem "
    }
    return str
}

fun main(args: Array<String>) {
    println("Write number of values: ")
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    val orig = DoubleArray(n)
    val smooth = DoubleArray(n)

    println("Write $n values: ")

    for (i in 0 until n) orig[i] = sc.nextDouble()
    for (i in 1 until n - 1) smooth[i] = (orig[i - 1] + orig[i] + orig[i + 1]) / 3.0
    smooth[0] = orig[0]
    smooth[n - 1] = orig[n - 1]

    val originalFile = File("original.txt")
    originalFile.writeText(arrayToOutput(orig))

    val smoothFile = File("smooth.txt")
    smoothFile.writeText(arrayToOutput(smooth))
}